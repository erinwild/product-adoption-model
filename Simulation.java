import java.io.*;
import java.math.*;
import java.util.*;
import org.apache.commons.math3.distribution.BetaDistribution;

public class Simulation {
    
    /* Setting simulation variables */
    public static final int popSize = 2000;
    public static final int timeSteps = 500;
    public static final int numRuns = 1; // number of times to repeat the simulation
    public static final boolean individualValues = true;
    public static final boolean network = false;
    public static final boolean sigmaFactor = false;
    public static final double initialUnaware = 0.97; // fraction of unaware in the population at t=0
    public static final double initialAware = 0.02; // fraction of aware in the population at t=0
    public static final double initialAdopter = 0.01; // fraction of adopters in the population at t=0
    public static ArrayList<Individual> population;
    
    public static int numU;
    public static int numI;
    public static int numX;
    public static double sumAwareContactRates;
    public static double sumAdopterContactRates;
    
    public static File fileStates;
    public static PrintWriter printerStates;
    
    public static void main(String[] args) throws FileNotFoundException {
        
        long startTime = System.nanoTime();
        
        for(int sim = 1; sim <= numRuns; sim++) {
            
            fileStates = new File("states" + sim + ".txt");
            printerStates = new PrintWriter(fileStates);
            
            numU = (int)Math.round(initialUnaware*popSize);
            numI = (int)Math.round(initialAware*popSize);
            numX = (int)Math.round(initialAdopter*popSize);
            if(numU + numI + numX != popSize) {
                throw new IllegalArgumentException("Warning: groups sizes do not add up to the total population size.");
            }
            
            MersenneTwisterFast rng = new MersenneTwisterFast();
            BetaDistribution betaRng = new BetaDistribution(2.0,2.0);
            
            /* Initializing population */
            population = new ArrayList<Individual>(popSize);
            sumAwareContactRates = 0;
            sumAdopterContactRates = 0;
            for(int i = 0; i < popSize; i++) {
                if(i < numX) {
                    population.add(new Individual(Individual.State.ADOPTER));
                } else if(i < numX + numI) {
                    population.add(new Individual(Individual.State.AWARE));
                } else {
                    population.add(new Individual());
                }
                /* Initializing individual attributes */
                if(individualValues) {
                    population.get(i).setAdvertisingEffect(betaRng.sample()/10);
                    population.get(i).setContactRate(0.0000316*betaRng.sample());
                    population.get(i).setAdoptionRate(0.11492*betaRng.sample());
                    population.get(i).setPerceivedCost(7000*betaRng.sample());
                    if(sigmaFactor) {
                        population.get(i).setSocialInfluence(betaRng.sample());
                    }
                    /* Updating sum of contact rates */
                    if(i < numX) {
                        sumAdopterContactRates += population.get(i).getContactRate();
                    } else if(i < numX + numI) {
                        sumAwareContactRates += population.get(i).getContactRate();
                    }
                }
            }
            Collections.shuffle(population);
            
            printCurrentState();
            
            /* Importing network from adjacency matrix to create neighbourhood for each Individual */
            if(network) {
                importAdjFile("adj_pa_2000.txt");
            }
            
            /* Time step */
            double advertising;
            double adopterInfluence;
            for(int t = 1; t <= timeSteps; t++) {
                if((double)t/timeSteps <= 0.3) {
                    advertising = -6*Math.pow(((double)t/timeSteps)-0.3,2)+0.6;
                } else {
                    advertising = (1/(Math.sqrt(2*Math.PI)))*(Math.exp((-20*Math.pow(((double)t/timeSteps)-0.3,2))/2))-(1/(Math.sqrt(2*Math.PI)))+0.6;
                }
                for(int i = 0; i < popSize; i++) {
                    /* Updating information about neighbours */
                    if(individualValues) {
                        if(network) {
                            population.get(i).categorizeNeighbours();
                            population.get(i).updateAvgContactRates();
                        } else {
                            population.get(i).categorizeNeighbours(numU,numI,numX);
                            population.get(i).updateAvgContactRates(sumAwareContactRates,sumAdopterContactRates,numI,numX);
                        }
                    } else {
                        population.get(i).categorizeNeighbours(numU,numI,numX);
                    }
                    /* Actions */
                    adopterInfluence = population.get(i).getAvgAdopterContactRate()*population.get(i).getNumAdopterNeighbours();
                    switch(population.get(i).getCurrentState()) {
                        case UNAWARE: /* Unaware Actions */
                            if(rng.nextDouble() < population.get(i).getAdvertisingEffect()*advertising + population.get(i).getAvgAwareContactRate()*(double)population.get(i).getNumAwareNeighbours()/population.get(i).getNumNeighbours(popSize) + population.get(i).getAvgAdopterContactRate()*(double)population.get(i).getNumAdopterNeighbours()/population.get(i).getNumNeighbours(popSize)) { // likelihood of becoming aware from the DE model
                                population.get(i).setNextState(Individual.State.AWARE);
                            }
                            break;
                        case AWARE: /* Aware Actions */
                            if(rng.nextDouble() < population.get(i).getAdoptionRate()*((Math.exp(-population.get(i).getPriceSensitivity()*population.get(i).getPerceivedCost()))*(1-population.get(i).getSocialInfluence())+population.get(i).getSocialInfluence()*adopterInfluence)) { // likelihood of becoming a potential adopter from the DE model and adopting
                                population.get(i).setNextState(Individual.State.ADOPTER);
                            }
                            break;
                        case ADOPTER: /* Adopter Actions */
                            if(rng.nextDouble() < population.get(i).getAdoptionRate()*(1-((Math.exp(-population.get(i).getPriceSensitivity()*population.get(i).getPerceivedCost()))*(1-population.get(i).getSocialInfluence())+population.get(i).getSocialInfluence()*adopterInfluence))) { // likelihood of finding the price unacceptable and disadopting
                                population.get(i).setNextState(Individual.State.AWARE);
                            }
                            break;
                    }
                }
                /* Transitioning to next state and updating information */
                for(int i = 0; i < popSize; i++) {
                    population.get(i).goToNextState();
                    numU = 0;
                    numI = 0;
                    numX = 0;
                    sumAwareContactRates = 0;
                    sumAdopterContactRates = 0;
                    switch(population.get(i).getCurrentState()) {
                        case UNAWARE: numU++;
                            break;
                        case AWARE: numI++;
                            sumAwareContactRates += population.get(i).getContactRate();
                            break;
                        case ADOPTER: numX++;
                            sumAdopterContactRates += population.get(i).getContactRate();
                            break;
                    }
                }
                printCurrentState();
            }
            printerStates.close();
        }
        
        long endTime = System.nanoTime();
        System.out.println(numRuns + " run(s) took " + (endTime - startTime) + " ns");
    }
    
    public static void importAdjFile(String filepath) throws FileNotFoundException {
        // Reads file to create network/neighbourhood for each Individual
        //  Expected Input: each row i should be a list of numbers corresponding to the individuals adjacent to Individual i
        File fileAdj = new File(filepath);
        Scanner scanAdj = new Scanner(fileAdj);
        Scanner scanStringAdj;
        int neighbourIndex;
        for(int i = 0; i < popSize; i++) {
            scanStringAdj = new Scanner(scanAdj.nextLine());
            while(scanStringAdj.hasNextInt()) {
                neighbourIndex = scanStringAdj.nextInt();
                if(neighbourIndex == i) {
                    throw new IllegalArgumentException("An Individual cannot have itself as a neighbour.");
                } else if(neighbourIndex < 0 || neighbourIndex >= popSize) {
                    throw new IllegalArgumentException("Neighbour must be valid index starting from 0 but strictly less than the population size.");
                } else {
                    population.get(i).addNeighbour(population.get(neighbourIndex));
                }
            }
        }
    }
    
    public static void printCurrentState() throws FileNotFoundException {
        // prints the current state (UNAWARE=0, AWARE=1, ADOPTER=2) of each Individual to .txt file
        for(int i = 0; i < popSize; i++) {
            switch(population.get(i).getCurrentState()) {
                case UNAWARE: printerStates.print("0 ");
                    break;
                case AWARE: printerStates.print("1 ");
                    break;
                case ADOPTER: printerStates.print("2 ");
                    break;
            }
        }
        printerStates.println();
    }
}