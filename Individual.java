import java.io.*;
import java.util.*;

public class Individual {
    public enum State {
        UNAWARE, AWARE, ADOPTER;
    }
    private State currentState;
    private State nextState;
    private ArrayList<Individual> neighbours = new ArrayList<Individual>(3); // neighbours are other Individuals connected (adjacent) to this Individual within a network
    private double c = 0.05; // advertising effect
    private double b = 0.0000158; // contact rate
    private double k = 0.05746; // quickness of adoption rate
    private double p = 3500; // personal perceived cost
    private double d = 0.00039; // price sensitivity
    private double sigma = 0; // social influence factor (likelihood to be influenced by neighbours who have adopted)
    private int numUnawareNeighbours = 0;
    private int numAwareNeighbours = 0;
    private int numAdopterNeighbours = 0;
    private double avgbAwareNeighbours = b; // average contact rate of aware neighbours
    private double avgbAdopterNeighbours = b; // average contact rate of adopter neighbours
    
    public Individual() {
        currentState = State.UNAWARE;
        nextState = currentState;
    }
    
    public Individual(State initialState) {
        currentState = initialState;
        nextState = currentState;
    }
    
    public Individual(State initialState, double advertisingEffect, double contactRate, double adoptionRate, double perceivedCost, double priceSensitivity) {
        currentState = initialState;
        nextState = currentState;
        c = advertisingEffect;
        b = contactRate;
        k = adoptionRate;
        p = perceivedCost;
        d = priceSensitivity;
    }
    
    public State getCurrentState() {
        return currentState;
    }
    
    public void setNextState(State newState) {
        nextState = newState;
    }
    
    public void goToNextState() {
        currentState = nextState;
    }
    
    public void setNeighbourhood(ArrayList<Individual> neighbourhood) {
        neighbours = neighbourhood;
    }
    
    public void addNeighbour(Individual newNeighbour) {
        neighbours.add(newNeighbour);
    }
    
    public ArrayList<Individual> getNeighbourhood() {
        return neighbours;
    }
    
    public void setAdvertisingEffect(double advertisingEffect) {
        if(advertisingEffect < 0) {
            throw new IllegalArgumentException("Advertising effect must be a positive number.");
        } else {
            c = advertisingEffect;
        }
    }
    
    public double getAdvertisingEffect() {
        return c;
    }
    
    public void setContactRate(double contactRate) {
        if(contactRate < 0) {
            throw new IllegalArgumentException("Contact rate must be a positive number.");
        } else {
            b = contactRate;
        }
    }
    
    public double getContactRate() {
        return b;
    }
    
    public void setAdoptionRate(double adoptionRate) {
        if(adoptionRate < 0) {
            throw new IllegalArgumentException("Adoption rate must be a positive number.");
        } else {
            k = adoptionRate;
        }
    }
    
    public double getAdoptionRate() {
        return k;
    }
    
    public void setPerceivedCost(double perceivedCost) {
        if(perceivedCost < 0) {
            throw new IllegalArgumentException("Perceived cost must be a positive number.");
        } else {
            p = perceivedCost;
        }
    }
    
    public double getPerceivedCost() {
        return p;
    }
    
    public void setPriceSensitivity(double priceSensitivity) {
        if(priceSensitivity < 0) {
            throw new IllegalArgumentException("Price sensitivity must be a positive number.");
        } else {
            d = priceSensitivity;
        }
    }
    
    public double getPriceSensitivity() {
        return d;
    }
    
    public void setSocialInfluence(double socialInfluence) {
        if(socialInfluence < 0 || socialInfluence > 1) {
            throw new IllegalArgumentException("Social influence must be between 0 and 1.");
        } else {
            sigma = socialInfluence;
        }
    }
    
    public double getSocialInfluence() {
        return sigma;
    }
    
    public void categorizeNeighbours() {
        // updates the number of neighbours currently in each state
        if(getNeighbourhood().isEmpty()) {
            throw new NullPointerException("No network has been specified, thus all individuals are assumed to be connected; use population totals instead.");
        } else {
            numUnawareNeighbours = 0;
            numAwareNeighbours = 0;
            numAdopterNeighbours = 0;
            for(Individual i : getNeighbourhood()) {
                switch(i.getCurrentState()) {
                    case UNAWARE: numUnawareNeighbours++;
                        break;
                    case AWARE: numAwareNeighbours++;
                        break;
                    case ADOPTER: numAdopterNeighbours++;
                        break;
                }
            }
        }
    }
    
    public void categorizeNeighbours(int numU, int numI, int numX) {
        // used to update number of neighbours if all Individuals are connected (and neighbours ArrayList is thus empty)
        numUnawareNeighbours = numU;
        numAwareNeighbours = numI;
        numAdopterNeighbours = numX;
        switch(getCurrentState()) {
            case UNAWARE: numUnawareNeighbours--;
                break;
            case AWARE: numAwareNeighbours--;
                break;
            case ADOPTER: numAdopterNeighbours--;
                break;
        }
    }
    
    public int getNumUnawareNeighbours() {
        return numUnawareNeighbours;
    }
    
    public int getNumAwareNeighbours() {
        return numAwareNeighbours;
    }
    
    public int getNumAdopterNeighbours() {
        return numAdopterNeighbours;
    }
    
    public int getNumNeighbours(int popSize) {
        if(neighbours.size() == 0) {
            return popSize-1;
        } else {
            return neighbours.size();
        }
    }
    
    public void updateAvgContactRates() {
        // updates the average contact rate for aware and adopter neighbours (with network)
        if(getNeighbourhood().isEmpty()) {
            throw new NullPointerException("No network has been specified, thus all individuals are assumed to be connected; use population totals instead.");
        } else {
            avgbAwareNeighbours = 0;
            avgbAdopterNeighbours = 0;
            for(Individual i : getNeighbourhood()) {
                switch(i.getCurrentState()) {
                    case UNAWARE:
                        break;
                    case AWARE: avgbAwareNeighbours += i.getContactRate();
                        break;
                    case ADOPTER: avgbAdopterNeighbours += i.getContactRate();
                        break;
                }
            }
            if(numAwareNeighbours != 0) {
                avgbAwareNeighbours = avgbAwareNeighbours/getNumAwareNeighbours();
            }
            if(numAdopterNeighbours != 0) {
                avgbAdopterNeighbours = avgbAdopterNeighbours/getNumAdopterNeighbours();
            }
        }
    }
    
    public void updateAvgContactRates(double sumAwareContactRates, double sumAdopterContactRates, int numI, int numX) {
        // updates the average contact rate for aware and adopter neighbours (no network)
        switch(getCurrentState()) {
            case UNAWARE:
                if(numI != 0) {
                    avgbAwareNeighbours = sumAwareContactRates/numI;
                } else {
                    avgbAwareNeighbours = 0;
                }
                if(numX != 0) {
                    avgbAdopterNeighbours = sumAdopterContactRates/numX;
                } else {
                    avgbAdopterNeighbours = 0;
                }
                break;
            case AWARE:
                if(numI > 1) {
                    avgbAwareNeighbours = (sumAwareContactRates-getContactRate())/(numI-1);
                } else {
                    avgbAwareNeighbours = 0;
                }
                if(numX != 0) {
                    avgbAdopterNeighbours = sumAdopterContactRates/numX;
                } else {
                    avgbAdopterNeighbours = 0;
                }
                break;
            case ADOPTER:
                if(numI != 0) {
                    avgbAwareNeighbours = sumAwareContactRates/numI;
                } else {
                    avgbAwareNeighbours = 0;
                }
                if(numX > 1) {
                    avgbAdopterNeighbours = (sumAdopterContactRates-getContactRate())/(numX-1);
                } else {
                    avgbAdopterNeighbours = 0;
                }
                break;
        }
    }
    
    public double getAvgAwareContactRate() {
        return avgbAwareNeighbours;
    }
    
    public double getAvgAdopterContactRate() {
        return avgbAdopterNeighbours;
    }
}